#!/usr/bin/env python

import json

import requests


def get_config():
    with open("config.json") as config_file:
        return json.load(config_file)


def get_public_ip():
    """ Return the actual current public IP address. """
    response = requests.get("https://api.ipify.org")
    response.raise_for_status()

    return response.text


def read_last_ip():
    """ Fetch the latest IP address. """
    with open("ip.txt") as last_ip:
        return last_ip.read().strip()


def update_last_ip(ip_address):
    with open("ip.txt", "w") as last_ip:
        last_ip.write(ip_address)


def send_email(old_ip, new_ip, config):
    body = config["email_template"].format(old_ip=old_ip, new_ip=new_ip)

    payload = {
        "to": config["to_address"],
        "from": config["from_address"],
        "subject": "Public IP address has changed to {new_ip}".format(new_ip=new_ip),
        "text": body,
    }

    response = requests.post(
        "https://api.mailgun.net/v3/{domain}/messages".format(domain=config["sending_domain"]),
        data=payload,
        auth=("api", config["mailgun_api_key"]),
    )
    print(response.text)
    response.raise_for_status()


def main():
    config = get_config()

    old_ip = read_last_ip()
    new_ip = get_public_ip()

    if old_ip != new_ip:
        print("IP has changed, sending email.")
        send_email(old_ip, new_ip, config)

    update_last_ip(new_ip)


if __name__ == "__main__":
    main()
